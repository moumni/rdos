import json

# Some JSON string created by the web site are impossible to parse by python-json library
# Thus, we replace special characters
# 'paramaters' is the JSON string
# Returns a dictionnary containing the key-values of the JSON string.
def trim_and_load_json(parameters:str):
    parameters = parameters.replace('\r','')
    parameters = parameters.replace('\n',' ')
    return json.loads(parameters)


# Create the commands that will be executed by the launcher container.
# Replaces parameters names by the values selected by the user.
def create_command(parameters_info: tuple, tool: str, path: str,command: str, parameters:str , job_dir:str):
    (parameters_basefile, parameters_default) = parameters_info
    param_dict = trim_and_load_json(parameters)
    command = command.replace("{path}", ".")
    
    for param in param_dict:
        value = param_dict[param]
        if len(value) == 0: # If the value is empty, replaces it by a default value
            value = parameters_default[tool][param] 
            #value = value.replace('\\\\n', ' ')
        if  len(parameters_basefile[tool][param]) == 0: # Check whether the parameter should be given through a file or not.
            command = command.replace('{'+param+'}', value)
        else:
            command = command.replace('{'+param+'}', os.path.join(volume_point,job_dir,parameters_basefile[tool][param]))
            with open(os.path.join(working_dir, job_dir,parameters_basefile[tool][param]), 'w') as file:
                file.write(value)
            file.close()
                
    command += " > "+os.path.join(volume_point,job_dir,"result.txt")
    print(command)
    return command
