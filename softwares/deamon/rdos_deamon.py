import mysql.connector
from get_docker_secret import get_docker_secret
import time, datetime
import os
import sys
sys.path.append(os.path.realpath('../'))
from database.database import *

import docker
import json
from rdos_command import *


################################### FILESYSTEM INTERACTIONS ###################################
# Returns a dictionnary containing the generator's name as a key and it's associated image as a value
def getGenerators(path: str):
    with open(path, 'r') as file:
        data = file.read()
    file.close()
    obj = json.loads(data)
    generators_path = {}
    for generator in obj['generators']:
        generators_path[str(generator["name"])] = ( str(generator["path"]),  str(generator["image"]))        
    return generators_path

#Creates a directory where the job's result will be written, but also some jobs parameters.
def create_directory(directory_name: str):
    try:
            os.stat(directory_name)
    except:
            os.mkdir(directory_name)
            print("Directory", directory_name," created")

# Create the commands that will be executed by the launcher container.
# Replaces parameters names by the values selected by the user.
def create_command(parameters_info: tuple, tool: str, path: str,command: str, parameters:str , job_dir:str):
    (parameters_basefile, parameters_default) = parameters_info
    parameters = parameters.replace('\r','')
    parameters = parameters.replace('\n',' ')
    param_dict = json.loads(parameters)
    command = command.replace("{path}", ".")
    
    for param in param_dict:
        value = param_dict[param]
        if len(value) == 0: # If the value is empty, replaces it by a default value
            value = parameters_default[tool][param] 
            #value = value.replace('\\\\n', ' ')
        if  len(parameters_basefile[tool][param]) == 0: # Check whether the parameter should be given through a file or not.
            command = command.replace('{'+param+'}', value)
        else:
            command = command.replace('{'+param+'}', os.path.join(volume_point,job_dir,parameters_basefile[tool][param]))
            with open(os.path.join(working_dir, job_dir,parameters_basefile[tool][param]), 'w') as file:
                file.write(value)
            file.close()
                
    command += " > "+os.path.join(volume_point,job_dir,"result.txt")
    print(command)
    return command

################################### MAIN PROGRAM ###################################

working_dir = "/home/david/rdos/"
if os.getenv("working_dir") != None:
    working_dir = os.getenv("working_dir")
volume_point = "/usr/local/"
generators = getGenerators("generators.json")
client = docker.from_env()
parameters_info = getParameters(generators)


while 1:
    database = database_connection()
    result = get_job(database)
    if (not result == None):
        (job_id, parametersJSON, directory, email, tool, idGenerator,outputFormat,command) = result
        if tool in generators:
            if apply_for_job(database, job_id) == 1 :
                create_directory(directory.replace("./", working_dir))
                print(directory)
                command = create_command(parameters_info, tool, generators[tool][0], command, parametersJSON, directory)
                client.containers.run(generators[tool][1], detach=True, environment= {"job_id":job_id, "command":command, "directory":directory, "email":email, "rdos_secret":get_password()}, volumes = {'/home/david/rdos/jobs/': {'bind': '/usr/local/jobs', 'mode': 'rw'}})
                print(command," appelée sur un nouveau conteneur ", generators[tool][1])
        else:
            print("Tool ",tool," Unknown")
                
            
    database.disconnect()
    time.sleep(1)
    


    
    
