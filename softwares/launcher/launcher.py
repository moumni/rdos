import os
import sys
sys.path.append(os.path.realpath('../'))

from mailer.mail_templates import *
from database.database import *

import time
from datetime import datetime
import json


def getConfiguration(path: str):
    with open(path, 'r') as file:
        data = file.read()

    return json.loads(data)
    
def create_directory(directory_name: str):
    try:
            os.stat(directory_name)
    except:
            os.mkdir(directory_name)
            print("Directory", directory_name," created")


job_id = os.getenv("job_id")
command= os.getenv("command")
directory= os.getenv("directory")
email= os.getenv("email")
configuration = getConfiguration("configuration.json")

if (not job_id == None) and (not command == None) :
    create_directory(directory.replace("./","/usr/local/"))
    print("Execution de la commande ", command)
    before = datetime.fromtimestamp(time.time())
    os.system(command)
    after = datetime.fromtimestamp(time.time())
    print("Tâche effectuée en ", after-before)
    
    url = os.path.join( configuration["url"], command.split(">")[1].split("jobs/")[1] )
    update_database(url, job_id, str(before), str(after))
    send_mail(email,job_id)
    

