From: RDOS <rdos@lipn.univ-paris13.fr>
To: RDOS User <{{email}}>
Subject: RDOS: Did you order some discrete objects?

Thank you for using Rdos.
Please download your results at the following address:
http://lipn.univ-paris13.fr/rdos/viewjob.php?lang=en&css=bioinfo&jobid={{jobid}}

Note that the files will be deleted in 48 hours.
