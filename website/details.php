<?php 
	include_once("php/visualization.php");
?>
<!DOCTYPE html>
<meta charset="utf-8">
<style>

body {
  font: 10px sans-serif;
  text-align:center;
}

.axis path,
.axis line {
  fill: none;
  stroke: #000;
  shape-rendering: crispEdges;
}

.x.axis path {
}

.line {
  fill: none;
  stroke: steelblue;
  stroke-width: 3.5px;
}

path.link {
  fill: none;
  stroke: #666;
  stroke-width: 1.5px;
}

marker#licensing {
  fill: green;
}

path.link.licensing {
  stroke: green;
}

path.link.resolved {
  stroke-dasharray: 0,2 1;
}

circle {
  fill: #ccc;
  stroke: #333;
  stroke-width: 1.5px;
}

text {
  font: 10px sans-serif;
  pointer-events: none;
}

text.shadow {
  stroke: #fff;
  stroke-width: 3px;
  stroke-opacity: .8;
}


</style>
<body>
<script type='text/javascript' src='js/jquery-1.9.1.min.js'></script>
<script src="js/d3.v3.js"></script>
    <?php 
	$type = "";
	if (isset($_REQUEST["type"]))
	{
		$type = $_REQUEST["type"];
	}
	$content = "";
	if (isset($_REQUEST["content"]))
	{
		$content = urldecode($_REQUEST["content"]);
	}
	
	if ($type=="walk")
	{
		$walk = parseWalk($content);
		?>
		<script>
		$(document).ready(
		function(){

		var margin = {top: 10, right: 10, bottom: 20, left: 40},
			width =  $(document).width() - margin.left - margin.right-40,
			height = $(document).height() - margin.top - margin.bottom-40;

		var x = d3.scale.linear()
			.range([0, width]);

		var y = d3.scale.linear()
			.range([height, 0]);

		var xAxis = d3.svg.axis()
			.scale(x)
			.orient("bottom");

		var yAxis = d3.svg.axis()
			.scale(y)
			.orient("left");

		var line = d3.svg.line()
			.x(function(d) { return x(d.x); })
			.y(function(d) { return y(d.y); });

		var svg = d3.select("body").append("svg")
			.attr("width", width + margin.left + margin.right)
			.attr("height", height + margin.top + margin.bottom)
		  .append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

		 
		var data = <?php echo formatWalk($walk);?>;

		  x.domain(d3.extent(data, function(d) { return (d.x); }));
		  y.domain(d3.extent(data, function(d) { return (d.y); }));
		  

		  svg.append("g")
			  .attr("class", "x axis")
			  .attr("transform", "translate(0," + height + ")")
			  .call(xAxis)
			  ;

		  svg.append("g")
			  .attr("class", "y axis")
			  .call(yAxis)
			.append("text")
			  .attr("transform", "rotate(-90)")
			  .attr("y", 6)
			  .attr("dy", ".71em")
			  .style("text-anchor", "end")
			  .text("Y");

		  svg.append("path")
			  .datum(data)
			  .attr("class", "line")
			  .attr("d", line);
		});
		</script>
		<?php
	}
	else if ($type=="automaton")
	{
		$graph = parseGraph($content);
		$json = formatGraph($graph);
		//echo $json;
		?>
		<script>
			var links = <?php echo $json; ?>;
			var nodes = {};

			// Compute the distinct nodes from the links.
			links.forEach(function(link) {
			  link.source = nodes[link.source] || (nodes[link.source] = {name: link.source});
			  link.target = nodes[link.target] || (nodes[link.target] = {name: link.target});
			});

			var w = $(document).width()-30,
				h = $(document).height()-30;

			var force = d3.layout.force()
				.nodes(d3.values(nodes))
				.links(links)
				.size([w, h])
				.linkDistance(60)
				.charge(-300)
				.on("tick", tick)
				.start();

			var svg = d3.select("body").append("svg:svg")
				.attr("width", w)
				.attr("height", h);

			// Per-type markers, as they don't inherit styles.
			svg.append("svg:defs").selectAll("marker")
				.data(["suit", "licensing", "resolved"])
			  .enter().append("svg:marker")
				.attr("id", String)
				.attr("viewBox", "0 -5 10 10")
				.attr("refX", 15)
				.attr("refY", -1.5)
				.attr("markerWidth", 6)
				.attr("markerHeight", 6)
				.attr("orient", "auto")
			  .append("svg:path")
				.attr("d", "M0,-4L10,0L0,5");

			var path = svg.append("svg:g").selectAll("path")
				.data(force.links())
			  .enter().append("svg:path")
				.attr("class", function(d) { return "link " + d.type; })
				.attr("marker-end", function(d) { return "url(#" + d.type + ")"; });

			var circle = svg.append("svg:g").selectAll("circle")
				.data(force.nodes())
			  .enter().append("svg:circle")
				.attr("r", 6)
				.call(force.drag);

			var text = svg.append("svg:g").selectAll("g")
				.data(force.nodes())
			  .enter().append("svg:g");

			// A copy of the text with a thick white stroke for legibility.
			text.append("svg:text")
				.attr("x", 8)
				.attr("y", ".31em")
				.attr("class", "shadow")
				.text(function(d) { return d.name; });

			text.append("svg:text")
				.attr("x", 8)
				.attr("y", ".31em")
				.text(function(d) { return d.name; });

			// Use elliptical arc path segments to doubly-encode directionality.
			function tick() {
			  path.attr("d", function(d) {
				var dx = d.target.x - d.source.x,
					dy = d.target.y - d.source.y,
					dr = Math.sqrt(dx * dx + dy * dy)+.5;
				return "M" + d.source.x + "," + d.source.y + "A" + dr + "," + dr + " 0 0,1 " + d.target.x + "," + d.target.y;
			  });

			  circle.attr("transform", function(d) {
				return "translate(" + d.x + "," + d.y + ")";
			  });

			  text.attr("transform", function(d) {
				return "translate(" + d.x + "," + d.y + ")";
			  });
			}

		</script>		
		<?php
	}
	else if ($type=="tree")
	{
		$tree = parseTree($content);
		$txt = json_encode($tree);
	?>
    <script type="text/javascript">

	var w = $(document).width()-30,
		h = $(document).height()-30;

	var treeData = <?php echo $txt;?>;
      // Create a svg canvas


 
	var tree = d3.layout.tree()
		.size([w - 40, h - 40]);

      var vis = d3.select("body").append("svg:svg")
      .attr("width", w)
      .attr("height", h) 
	  .append("g")
.attr("transform", "translate(20,20)");
		
      var diagonal = d3.svg.diagonal()
      // change x and y (for the left to right tree)
      .projection(function(d) { return [d.x, d.y]; });
 
      // Preparing the data for the tree layout, convert data into an array of nodes
      var nodes = tree.nodes(treeData);
      // Create an array with all the links
      var links = tree.links(nodes);
 
 
      var link = vis.selectAll("pathlink")
      .data(links)
      .enter().append("svg:path")
      .attr("class", "link")
      .attr("d", diagonal)
 
      var node = vis.selectAll("g.node")
      .data(nodes)
      .enter().append("svg:g")
      .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; })
 
      // Add the dot at every node
      node.append("svg:circle")
      .attr("r", 5);
 
      // place the name atribute left or right depending if children
      node.append("svg:text")
      .attr("dx", function(d) { return d.children ? 10 : -4; })
      .attr("dy", function(d) { return d.children ? 3 : 15; })
      //.attr("text-anchor", function(d) { return d.children ? "end" : "start"; })
      .text(function(d) { return d.name; })
    </script>	
	<?php
	}
	else //if ($type=="sequence")
	{
		print("<div class=\"sequence\">$content</div>");
	}
?>