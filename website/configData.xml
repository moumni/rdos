<?xml version="1.0" encoding="UTF-8"?>
<toolconfig>
  <!-- General/primary name of the tool -->
  <tool><![CDATA[GenRGenS]]></tool>
  
  <!-- Secondary name/execution mode of the generator -->
  <mode><![CDATA[Markov Generation]]></mode>
  
  <!-- Description (possibly long) in French -->
  <description-FR><![CDATA[Un outil pour la génération aléatoire de séquences en mode "poisson rouge".]]></description-FR>
  
  <!-- Description (possibly long) in English -->
  <description-EN><![CDATA[A tool for the random generator of strange and funny stuff.]]></description-EN>
  
  <!-- Comma-separated list of authors -->
  <authors><![CDATA[Anne Heaunyme, Harry Kohver, Bill Beaucquet]]></authors>
  
  <!-- URL pointing towards software home page, or [main] contributor web  page -->
  <url><![CDATA[http://www.github.com/MySoft]]></url>
  
  <!-- Description of publications/patents... related to the software, either provided as a full (HTML) text, or a comma-separated list of HAL identifiers pointing towards the document(s). -->
  <reference><![CDATA[inria-00548871,hal-00643598]]></reference>
  
  <!-- 
    Type of objects produced by the generator. This field is used to determine which visualization can be applied. It may be any of the following:
	-'sequence': Simple sequence (please use this type as default if your type of object is not supported)
	-'automaton': General automaton (online visualization snippets currently only support DOT formatted automata)
	-'tree': General tree (online visualization snippets only support functional "Maple-style" encodings for trees, such as "A(Y,Z,B(X,C(X,Y,Z)),C(X,Y,Z))")
	-'permutation': General permutation (Space separated list of numbers)
	-'walk': General 2D walk (Space separated list of (dx,dy) pairs, such as "(1,1) (1,-1) (-3,3)")
	-'graph': General Graph (online visualization snippets currently only support DOT formatted graphs)
  -->
  <outputFormat><![CDATA[sequence]]></outputFormat>
  
  <!-- Random generation paradigm, chosen among:
	-'Ad hoc' 
    -'Recursive Generation'
	-'Boltzmann sampling'
	-'MCMC'
	-'Perfect sampling'
	Please use 'Ad hoc' as the default if you cannot relate your generator to any other, and do not hesitate to contact us if some category seems missing.
  -->
  <paradigm><![CDATA[Recursive Generation]]></paradigm>
  
  <!-- 
    Bound on approximate time-consumption, expressed as a function of the input parameters (see below).
	This will be used to set a threshold on the admissible parameters values.
	Variables (Nested by braces) correspond to the names of input parameters. 
	Here, the generation is linear on the size of generated sequences, and free of preprocessing.
	We accept the generation as long as the product of size and #objects does not exceed 1000, 
	i.e. one object of size <=1000, two of size <=500...
  -->
  <consumptionLimit><![CDATA[{numberObjects}*{size} <= 1000]]></consumptionLimit>
  
  <!-- 
    Command-line that should be typed/executed from a terminal to run the generator with specified parameters. 
	As described above, variables (Nested by braces) correspond to the names of input parameters.
	The user-provided content of a parameter named 'xxx' will thus be available in a variable named {xxx}.
  -->
  <command><![CDATA[java -cp . {modelFile} -num {number} -size {size}]]></command>
  
  <!-- 
    This section describes a parameter that will be passed to the software through the command line.
	The user-provided content of a parameter named 'size' will be available in a variable named {size}.
	Alternatively, a 'baseFile' may be specified (see below), in which case the user-provided content of the variable 
	will be dumped to the path found in the 'baseFile' option, and the {size} variable would refer to the path of the actual local file.
	This mode would be particularly useful, e.g. in cases where the generator expects an input file.
  -->
  <parameter>
    <!-- 
	  The name of the parameter. The name should be unique, and not contain any whitespace. 
	  It will be used to refer to the user specified values (see consumptionLimit and command options above).
	 -->
    <name><![CDATA[size]]></name>
    <!-- 
	  Specifies the rank of the parameter in the HTML-formatted input form.
	  Here, this value means that the parameter should appear first in the automatically generated input form.
	  -->
    <rank><![CDATA[1]]></rank>
    <!-- Specifies a French user-friendly caption for the parameter -->
    <caption-FR><![CDATA[Taille des séquences]]></caption-FR>
    <!-- Specifies an English user-friendly caption for the parameter -->
    <caption-EN><![CDATA[Sequence length]]></caption-EN>
    <!-- 
	  The (loose) type of this parameter, currently mostly used for validation and HTML formatting purposes.
	  Currently available types are 'integer','real','shortText','longText', and 'enum'.
	  The latter 'enum' parameter means that the values option will be interpreted as a restrictive list of possible values for the parameter.
	-->
    <type><![CDATA[integer]]></type>
	<!-- 
	  Default value for the parameter. 
	  Here, let us set the default length to 50.
	-->
    <defaultValue><![CDATA[50]]></defaultValue>
	<!-- 
	  The syntax and semantics of the 'values' option depends on the type of parameter:
	  - For 'enum'-typed parameters, the content of values will be a comma-separated list of possible values. 
	    (ex.:"yes,no" for a boolean switch, or "xml,dot,txt" for a format selection)
	  - For other parameters, bounds can be specified through a "min=xxx,max=yyy" syntax (ex.: "min=1,max=10000" to limit sequence length)
	-->
    <values><![CDATA[min=1,max=10000]]></values>
	<!--
	  	If empty, the value of this parameter will be found in variable "{&name}" (&name being the parameter name, defined above) in the command-line.
		Otherwise, the 'baseFile' designates a relative path to a file where the user-provided content of the parameter
		will be dumped. In this case, the {&name} variable refers to the path of the actual local file.
	  	Here, we leave this field empty, and the parameter will be used directly in the command-line.
	-->
    <baseFile><![CDATA[]]></baseFile>
	<!--
	  	An HTML-formatted French help for this parameter.
	-->
    <help-FR><![CDATA[Taille de la séquence, inférieure à 10 000.]]></help-FR>
	<!--
	  	An HTML-formatted English help for this parameter.
	-->
    <help-EN><![CDATA[Sequence length (Only sequence smaller than 10 000 are supported)]]></help-EN>
  </parameter>
  
  
  <!-- 
    This section describes another parameter {number}.
  -->
  <parameter>
    <!-- 
	  The name of the parameter. 
	  The name should be unique, and not contain any whitespace. 
	  It will be used to refer to the user specified values (see consumptionLimit and command options above).
	 -->
    <name><![CDATA[number]]></name>
    <!-- 
	  Specifies the rank of the parameter in the HTML-formatted input form.
	  Here, this value means that the parameter should appear second in the automatically generated input form.
	  -->
    <rank><![CDATA[2]]></rank>
    <!-- Specifies a French user-friendly caption for the parameter -->
    <caption-FR><![CDATA[Nombre de séquences]]></caption-FR>
    <!-- Specifies an English user-friendly caption for the parameter -->
    <caption-EN><![CDATA[#sequences]]></caption-EN>
    <!-- 
	  The (loose) type of this parameter, currently mostly used for validation and HTML formatting purposes.
	  Currently available types are 'integer','real','shortText','longText', and 'enum'.
	-->
    <type><![CDATA[integer]]></type>
	<!-- 
	  Default value for the parameter. 
	  Here, let us set the default number of generated sequences to 20.
	-->
    <defaultValue><![CDATA[20]]></defaultValue>
	<!-- 
	  At least one sequence, and at most 100 sequences will be generated.
	-->
    <values><![CDATA[min=1,max=100]]></values>
	<!--
	  	Here, we leave this field empty, and the parameter will be used directly in the command-line.
	-->
    <baseFile><![CDATA[]]></baseFile>
	<!--
	  	An HTML-formatted French help for this parameter.
	-->
    <help-FR><![CDATA[Nombre de séquences engendrées, inférieur à 100.]]></help-FR>
	<!--
	  	An HTML-formatted English help for this parameter.
	-->
    <help-EN><![CDATA[Number of generated sequences (At most 100 can be generated in a single run)]]></help-EN>
  </parameter>
  
  
  <!-- 
    This section describes the Markov model {modelFile}, to be output to a file.
	This parameter will illustrate usage of the baseFile option.
  -->
  <parameter>
    <!-- 
	  The name of the parameter. The name should be unique, and not contain any whitespace. 
	  It will be used to refer to the user specified values (see consumptionLimit and command options above).
	 -->
    <name><![CDATA[modelFile]]></name>
    <!-- 
	  Specifies the rank of the parameter in the HTML-formatted input form.
	  Here, this value means that the parameter should appear third in the automatically generated input form.
	  -->
    <rank><![CDATA[3]]></rank>
    <!-- Specifies a French user-friendly caption for the parameter -->
    <caption-FR><![CDATA[Modèle aléatoire]]></caption-FR>
    <!-- Specifies an English user-friendly caption for the parameter -->
    <caption-EN><![CDATA[Random model]]></caption-EN>
    <!-- 
	  The (loose) type of this parameter, currently mostly used for validation and HTML formatting purposes.
	  Currently available types are 'integer','real','shortText','longText', and 'enum'.
	-->
    <type><![CDATA[longText]]></type>
	<!-- 
	  Default value for the parameter. 
	  Here, let us set the default model to a typical GenRGenS file.
	-->
    <defaultValue><![CDATA[TYPE = MARKOV
ORDER = 1
PHASE = 1
SYMBOLS = LETTERS
FREQUENCIES =
 AA 10 AC 25 AG 33 AT 32
 CA 20 CC 25 CG 23 CT 32
 GA 10 GC 15 GG 33 GT 42
 TA 10 TC 35 TG 33 TT 22
]]></defaultValue>
	<!-- 
	  No particular constraints here.
	-->
    <values><![CDATA[]]></values>
	<!--
	  	Specifying a file name 'mymodel.ggd' here means that the content of this parameter will be dumped to a file named 'mymodel.ggd' prior to running the software.
		Additionally, the variable {modelFile} will be set to contain the path of the file, and will be usable within the command definition and limit time consumption.
	-->
    <baseFile><![CDATA[mymodel.ggd]]></baseFile>
	<!--
	  	An HTML-formatted French help for this parameter.
	-->
    <help-FR><![CDATA[Nombre de séquences engendrées, inférieur à 100.]]></help-FR>
	<!--
	  	An HTML-formatted English help for this parameter.
	-->
    <help-EN><![CDATA[Number of generated sequences (At most 100 can be generated in a single run)]]></help-EN>
  </parameter>
</toolconfig>
