<?php
	function parseWalk($content)
	{
		$result = array();
		$data = explode(" ", $content);
		$x = 0;
		$y = 0;
		if (count($data)==1)
		{
			for($i=0;$i<strlen($content);$i++)
			{
				$c = $content[$i];
				$dx = 0;
				$dy = 0;
				if ($c=='n')
				{
					$dy=1;
				}
				else if ($c=='s')
				{
					$dy=-1;
				}
				else if ($c=='e')
				{
					$dx=1;
				}
				else if ($c=='w')
				{
					$dx=-1;
				}
				$x = $x+$dx;
				$y = $y+$dy;
				$result[] = array("x"=>$x,"y" =>$y);
			}
		}
		else
		{
			$result[] = array("x"=>$x,"y" =>$y);
			foreach($data as $str)
			{
				$pair = explode(",", $str);
				if (count($pair)!=2)
				{
					return $result;
				}
				$dx = intval(substr($pair[0],1));
				$dy = intval(substr($pair[1],0,strlen($pair[1])-1));
				$x = $x + $dx;
				$y = $y + $dy;
				$result[] = array("x"=>$x,"y" =>$y);
			}
		}
		return $result;
	}


	
	function formatWalk($walk)
	{
		$result = "";
		foreach($walk as $p)
		{
			if ($result!= "")
			{
				$result .= ",";
			}
			$x = $p["x"];
			$y = $p["y"];
			$result .= "{x:$x,y:$y}";
		}
		return "[$result]";
	}

	function parseGraph($content)
	{
		$result = array();
		$content = substr($content,strpos($content,"{")+1);
		$data = explode("->", $content);
		$nodesHash = array();
		$nodes = array();
		$edges = array();
		(preg_match_all('/(?P<nodeFrom>[a-zA-Z0-9\"]+)[\s]*->[\s]*(?P<nodeTo>[a-zA-Z0-9\"]+)[\s]*(?P<style>\[.*?\])?/', $content, $matches));  
		
		for($k=0;$k<count($matches["nodeFrom"]);$k+=1)
		{
			$from = str_replace("\"","",$matches["nodeFrom"][$k]);
			$to = str_replace("\"","",$matches["nodeTo"][$k]);
			$style = $matches["style"][$k];
			$edges[] = array("source"=>$from, "target" => $to, "type"=>"suit");			
		}
		return $edges;
	}
	
	function formatGraph($graph)
	{
		$txt = json_encode($graph);
		return $txt;
	}

	function parseTree($content,&$i=0,$open = "(",$close = ")",$sep=",")
	{
		$result = array();
		$token= "";
		$finished = false;
		while(($i<strlen($content)) and !$finished)
		{
			$c = $content[$i];
			if ($c!=$close  and $c!=$open and $c!=$sep)
			{
				$token .= $c;
				$i++;
			}
			else
			{
				$finished = true;
			}
		}
		$finished = false;
		$children = array();
		if ($c == $open)
		{
			while(($i<strlen($content)) and !$finished)
			{
				$c = $content[$i];
				$i++;
				if ($c==$open or $c==$sep)
				{
					$children[]=parseTree($content,$i,$open,$close,$sep);		
				}
				else if ($c==$close)
				{
					$finished = true;
				}
			}
		}
		$result["name"] = $token;
		if (count($children)!=0)
		{ $result["children"] = $children; } 
		return $result;
	}
?>