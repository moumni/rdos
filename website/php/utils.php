<?php
  $flags = array(
    'fr' => '<img src="pics/smflagfr.png" ALT="French flag">',
    'en' => '<img src="pics/smflagen.png" ALT="English flag">',
    'FR' => '<img src="pics/smflagfr.png" ALT="French flag">',
    'EN' => '<img src="pics/smflagen.png" ALT="English flag">',
  );
  $DS = '/';

  function endsWith( $str, $sub ) {
    return ( substr( $str, strlen( $str ) - strlen( $sub ) ) == $sub );
  }

  function startsWith( $str, $sub ) {
    return ( substr( $str,0,strlen( $sub ) ) == $sub );
  }



  function populateBlockContent($category,$XMLFile,$template,$opts,$limitLow=-1,$limitHigh=PHP_INT_MAX)
  {
    $dynContent = loadDynamicContents($XMLFile,$opts,$limitLow,$limitHigh);
    foreach( $dynContent as $index => $content )
    {
      $template->assign_block_vars($category, $content->getAllLocData());
    }
  }


  function cleanup($xmlContent)
  {
    $beg = strpos($xmlContent,'>');
    $end = strrpos($xmlContent,'<');
    return substr($xmlContent,$beg+1,$end-$beg-1);
  }

  function explodePath($path)
  {
  }

  function getExternalFileContent($url)
  {
    if (startsWith( $url, "http" ) and function_exists("curl_init") and ($_SERVER['SERVER_NAME']!="127.0.0.1"))
    {
      $proxy_ip = 'kuzh.polytechnique.fr'; //proxy IP here
      $proxy_port = 8080; //proxy port from your proxy list
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_HEADER, false); // no headers in the output
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // output to variable
      curl_setopt($ch, CURLOPT_PROXYPORT, $proxy_port);
      curl_setopt($ch, CURLOPT_PROXYTYPE, 'HTTP');
      curl_setopt($ch, CURLOPT_PROXY, $proxy_ip);
      $response = curl_exec($ch);
      curl_close($ch);
      return($response);
    }
    else
    {
      return file_get_contents($url);
    }
  };

  function populateBiblio($url,$template,$opts,$shortDates=true)
  {
        basicLoadNestedBlockContent($url,$template,$opts,"");
  }


?> 