<?php 
	function createDir($dir, $prefix='', $mode=0777)
	{
		if (substr($dir, -1) != '/') 
			$dir .= '/';
		$path = $dir.$prefix.mt_rand(0, 9999999);
		do
		{
		 	$path = $dir.$prefix.mt_rand(0, 9999999);
		} while (!mkdir($path, $mode));
		//mkdir($path, $mode);
		return $path;
	}
	
	function dumpParamContent($dir,$content,$baseName)
	{
		$path  = $dir."/".$baseName;
		file_put_contents($path, $content);
		return $path;
	}
	
	function dumpFileParams($dir,$params)
	{
		foreach($params as $param)
		{
			if ($param["baseFile"]!="")
			{
				dumpParamContent($dir,$param["val"],$param["baseFile"]);
			};
		}
	}


	function getGenParams($dbh,$idgen,$values=array())
	{
		
		$sql = "SELECT * FROM parameters WHERE idGenerator = $idgen;";
		$dbresult = $dbh->query($sql);
		$result = array();
		if ($dbresult)
		{
			while($data = $dbresult->fetch_assoc())
			{
				if (count($values)==0)
				{
					$res = $data;
					$result[] = $res;
				}
				else if (isset($values["field".$data["id"]]))
				{
					$res = $data;
					$res["val"] = $values["field".$data["id"]];
					$result[] = $res;
				}
			}
		}
		return $result;
	}


	function getGenParamsAsJSON($params)
	{
		$toEncode = array();
		foreach($params as $param)
		{
			//echo $param["name"]." => ".$param["val"]."<br>";
			$toEncode[$param["name"]] = $param["val"];
		}
		$json = json_encode($toEncode);
		return $json;
	}
	
	function getIP() 
	{
		$ip;
		if (getenv("HTTP_CLIENT_IP"))
			$ip = getenv("HTTP_CLIENT_IP");
		else if(getenv("HTTP_X_FORWARDED_FOR"))
			$ip = getenv("HTTP_X_FORWARDED_FOR");
		else if(getenv("REMOTE_ADDR"))
			$ip = getenv("REMOTE_ADDR");
		else
			$ip = "UNKNOWN";
		return $ip;
	} 
	
	function addJob($dbh,$idgen,$email,$values)
	{	
		global $jobsDir;
		if (!file_exists($jobsDir))
		{
			mkdir($jobsDir,0777);
		}
		$jobdir = createDir($jobsDir);
		$params = getGenParams($dbh,$idgen,$values);
		$json = getGenParamsAsJSON($params);
		dumpFileParams($jobdir,$params);
		$jobid = uniqid();
		$sql = "INSERT INTO jobs(`id`,`idGenerator`,`IP`,`parametersJSON`, `status`, `directory`, `email`) VALUES ('$jobid','$idgen','".getIP()."','$json',".JOB_VALIDATED.",'$jobdir','$email');";
		$dbh->query($sql);
		return $jobid;
	}

	function getJobList($dbh)
	{
		$sql = "SELECT * FROM jobs";
		$dbresult = $dbh->query($sql);
		$result = array();
		if ($dbresult)
		{
			if($data = $dbresult->fetch_assoc())
			{
				return $data;
			}
		}
		return false;		
	}

	
	function getJobData($dbh,$jobid)
	{
		$sql = "SELECT * FROM jobs WHERE id = '$jobid';";
		$dbresult = $dbh->query($sql);
		$result = array();
		if ($dbresult)
		{
			if($data = $dbresult->fetch_assoc())
			{
				return $data;
			}
		}
		return false;		
	}

	function getGeneratorData($dbh,$genid)
	{
		$sql = "SELECT * FROM generators WHERE id = '$genid';";
		$dbresult = $dbh->query($sql);
		$result = array();
		if ($dbresult)
		{
			if($data = $dbresult->fetch_assoc())
			{
				return $data;
			}
		}
		return false;		
	}

	
	function formatCommandLine($dbh,$jobdata,$gendata)
	{
		$locjson = trim(preg_replace('/\s\s+/', '\n',$jobdata["parametersJSON"]));
		$paramValues = json_decode($locjson,True);
		$command = $gendata["command"];
		$genid = $gendata["id"];
		$params = getGenParams($dbh,$genid);
		$baseFiles = array();
		foreach($params as $par)
		{
			if ($par["baseFile"]!="")
				$baseFiles[$par["name"]] = $par["baseFile"];
		}
		foreach($paramValues as $name => $val)
		{
			if (isset($baseFiles[$name]))
			{	
				$val = $baseFiles[$name];
			}
			$command = str_replace('{'.$name.'}',$val,$command);
		}
		return $command;
	}
	
	function getStatusMsgs($status,$lang)
	{
		$result = array();
		switch ($status) {
		case JOB_DELETED:
			if ($lang=="fr")
				$result[] = "Tâche désormais indisponible";
			else
				$result[] = "Job no longer available";
		case JOB_DONE:
			if ($lang=="fr")
				$result[] = "Tâche executée";
			else
				$result[] = "Job finished";
		case JOB_OUTPUT:
			if ($lang=="fr")
				$result[] = "Formattage des données en sortie";
			else
				$result[] = "Formatting generator output";
		case JOB_RUNNING:
			if ($lang=="fr")
				$result[] = "Execution de la tâche";
			else
				$result[] = "Executing job";
		case JOB_QUEUED:
			if ($lang=="fr")
				$result[] = "Ajouté à la file d'attente (en attente de ressources disponibles)";
			else
				$result[] = "Job scheduled for execution (waiting for available computational ressources)";
		case JOB_VALIDATED:
			if ($lang=="fr")
				$result[] = "Validation des paramètres d'exécution";
			else
				$result[] = "Validating execution parameters";
		case JOB_CREATED:
			if ($lang=="fr")
				$result[] = "Création des répertoires";
			else
				$result[] = "Created directory";
			
		}
		return $result;
	}
	
	function formatStatus($status,$lang)
	{
		$result = "";
		$messages = getStatusMsgs($status,$lang);
		$firsttxt  = "current";
		foreach($messages as $msg)
		{
			$result = "<div class=\"jobStatusMessage $firsttxt\">$msg</div>".$result;
			$firsttxt  = "";
		}
		return $result;
	}
	
	function formatParameters($json)
	{
		$locjson = trim(preg_replace('/\s\s+/', '\n',$json));
		$paramValues = json_decode($locjson,True);
		$result = '<table class="parametersSummary">';
		foreach($paramValues as $key => $val)
		{
			$result .= "<tr><th>".ucfirst($key)."</th><td>$val</td></tr>\n";
		}
		$result .= '</table>';
		return $result;
	}
	
	function updateJobURL($dbh,$jobid,$newpath)
	{
		$sql = "UPDATE jobs SET url='".addslashes($newpath)."' WHERE jobid='$jobid';";
		$dbh->query($sql);
	}
	function fetchResults($dbh,$jobid,$directory)
	{
		global $jobsDir;
		$data = getJobData($dbh,$jobid);
		$path = $data["url"];
		if (!startsWith( $path, $jobsDir ))
		{
			if (!startsWith( $path, "http://" ))
			{
				$newpath = $directory."/".basename($path);
				if (copy($path,$newpath))
				{
					updateJobURL($dbh,$jobid,$newpath);
					return $newpath;
				}
			}
		}
		return $path;
	}
	
	function shorten($str,$lim=160)
	{
		if (strlen($str)>$lim)
		{
			$str = substr($str,0,$lim-3)."...";
		}
		return $str;
	}
	
	function formatResults($template,$url)
	{
		
		$lines = file($url);
		foreach($lines as $line_num => $line)
		{
			$line = trim($line);
			$template->assign_block_vars("randomObject",array(
				"content" => shorten($line),
				"contenturl" => urlencode($line),
				"id" => $line_num,
			));
		}
	}
?>