<?php

class DataSource {
    var $src = "";
    var $ignoreLevel=0;
    var $requiredFields=array();
    var $maxNumRecords=1000000;
	var $contentName = "element";
	var $hierarchy = array();
    function DataSource($src, $ignoreLevel=0, $requiredFields=array(),$maxNumRecords=1000000, $contentName="element", $hierarchy = array())
    {
      $this->src = $src;
      $this->ignoreLevel = $ignoreLevel;
      $this->requiredFields = $requiredFields;
      $this->maxNumRecords = $maxNumRecords;
      $this->contentName = $contentName;
	  $this->hierarchy = $hierarchy;
    }
}


class DynContent {
    var $data = array();
    var $opts = 0;
    var $id = 0;
    var $date = 0;
    var $children = array();
    var $father =  NULL;

    function DynContent($opts)
    {
        $this->opts = $opts;
    }

    function getData($key)
    {
      $key = strtolower($key);
      if (isset($this->data[$key]))
      {
        return $this->data[$key];
      }
      else
      { return ""; }
    }

    function setData($key,$value)
    {
      $key = strtolower($key);
      $tab = explode("-",$key);
      $l = count($tab);
      if ($l==2)
      {
        $key = $tab[0];
        $lan = $tab[1];
        if (strcmp($lan,$this->getLanguage()) && (isset($this->data[$key])))
        {
          return;
        }
      }
      else if ($l>2) return;
      if (strpos($key,'date')!==False)
      { $this->date = strtotime($value); }
      else
      { $this->data[$key] = $value; }
    }

    function getLocData($key)
    {
      $res = $this->getData($key);
      if (strcmp($res,"")==0)
      {return $this->getData($key);}
      else
      {return $res;}
    }

    function getID()
    {
      return $this->id;
    }

    function setID($id)
    {
      $this->id = $id;
    }

    function setChildren($a)
    {
      $this->children = $a;
    }

    function getChildren()
    {
      return $this->children;
    }

    function setFather($dynCont)
    {
      $this->father = $dynCont;
    }

    function getFather()
    {
      return $this->father;
    }


    function getCompactDate()
    {
      return date('Y-m-d',$this->date);
    }

    function getDate()
    {
      $enMonths = array("January", "February", "March", "April", "May", "June", "July", "September", "October", "November", "December");
      $frMonths = array("Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Septembre", "Octobre", "Novembre", "Décembre");
      $str = date('j F Y',$this->date);
      if ($this->isFrench())
      {
        $str = str_replace($enMonths,$frMonths,$str);
      }
      return $str;
    }

    function getLanguage()
    { return strtolower($this->opts->getLanguage()); }

    function isEnglish()
    { return !strcmp($this->getLanguage(),'en'); }

    function isHidden()
    {
      $res = $this->getData("hide");
      if (strcmp($res,"")==0)
      {return 0;}
      else
      {return 1;}
    }

    function isFrench()
    { return !strcmp($this->getLanguage(),'fr'); }

    function getURL()
    {
      $url = $this->getURLLoc();
      $needle = "http";
      if ((strpos($url, $needle) !== FALSE)||(strpos($url, "/") !== FALSE))
      { return $url; }
      else
      { return $this->opts->getURLForPage($url); }
    }
    function getURLLoc()
    { return $this->getLocData('url'); }
    function getAllLocData()
    {
      global $flags;
      $tab = array_merge($this->data);
      $tab['date'] = $this->getDate();
      $tab['shortdate'] = $this->getCompactDate();
      $tab['id'] = $this->getID();
      $tab['url'] = $this->getURL();
      $contentLang = strtolower($this->getData('language'));
      if (isset($flags[$contentLang]))
        $tab['flag'] = $flags[$contentLang];
      else
        $tab['flag'] = $flags[$this->opts->getLanguage()];
      return $tab;
    }
    function search($id)
    {
       if (!strcmp($this->getID(),$id))
       { return $this; }
       foreach($this->getChildren() as $index => $dyncont)
       {
         $tmp = $dyncont->search($id);
         if ($tmp!=NULL)
         {
           return $tmp;
         }
       }
       return NULL;
     }

}

function formatTXT($s)
{
   $from = array("\n");
   $to = array("\\n");
   return str_replace($from,$to,$s);
}

function formatURL($url,$opts)
{
    $needle = "http";
    if ((strpos($url, $needle) !== FALSE)||(strpos($url, "/") !== FALSE))
    { return $url; }
    else
    { return $opts->getURLForPage($url); }
}

function basicApplyNestedBlockContent($template,$xml,$opts,$ignoreLevel,$prefix="",$htmlprefix="",$htmlsuffix="",$required=array(),&$maxNumRecords=1000000)
{
  if ($maxNumRecords>0)
  {
  if ($ignoreLevel<=0)
  {
    if (strlen($prefix)>0)
    {$prefix = $prefix.".";}
    $prefix = $prefix.$xml->getName();
    $vals = array();
    foreach($xml->children() as $child)
    {
      if (count($child->children())==0)
      {
        if ($child->getName()!="url")
        {
          $vals[$opts->localize($child->getName())] = $htmlprefix.html_entity_decode(formatTXT(cleanup($child->asXML()))).$htmlsuffix;
        }
        else
        {
          $vals[$opts->localize($child->getName())] = $htmlprefix.html_entity_decode(formatURL(cleanup($child->asXML()),$opts)).$htmlsuffix;
        }
      }
    }
    $showRecord = true;
    if (isset($required[$prefix]))
    {
      $mandFields = $required[$prefix];
      foreach($mandFields as $f)
      {
        if (!isset($vals[$f]) || empty($vals[$f]))
          $showRecord = false;
      }
    }
    if ($showRecord)
    {
      $template->assign_block_vars($opts->localize($prefix),$vals);
      $maxNumRecords -= 1;
    }
  }
  foreach($xml->children() as $child)
  {
    if (count($child->children())!=0)
    { basicApplyNestedBlockContent($template,$child,$opts,$ignoreLevel-1,$prefix,$htmlprefix,$htmlsuffix,$required,$maxNumRecords); }
  }
  }
}

function basicLoadNestedBlockContent($dbh,$includeOpts,$lang)
{
	if (startsWith($includeOpts->src,"sql:"))
	{
		$content = createAsXML($dbh,substr($includeOpts->src,4),$includeOpts->contentName,$includeOpts->hierarchy,$lang);
	}
	else
	{
		$content = getExternalFileContent($includeOpts->src);
	}
	$xml = simplexml_load_string($content,'SimpleXMLElement',LIBXML_NOCDATA);
	return $xml;
}

$TypesPublis = array(
  "document_ART_ACL" => array(
    "title-en" => "Peer-reviewed journal articles",
    "title-fr" => "Revues avec comité de lecture",
    "rank" => 10,
    ),
  "document_COMM_ACT" => array(
    "title-en" => "Conferences with proceedings",
    "title-fr" => "Conférences avec comité de lecture et actes",
    "rank" => 20,
    ),
  "document_COMM_SACT" => array(
    "title-en" => "Conferences",
    "title-fr" => "Conférences avec comité de lecture",
    "rank" => 30,
    ),
  "document_COVS" => array(
    "title-en" => "Book chapters",
    "title-fr" => "Chapitres dans des ouvrages collectifs",
    "rank" => 40,
    ),
  "document_THESE" => array(
    "title-en" => "Theses",
    "title-fr" => "Thèses",
    "rank" => 45,
    ),
  "document_CONF_INV" => array(
    "title-en" => "Invited talks",
    "title-fr" => "Exposés invités",
    "rank" => 47,
    ),
  "document_REPORT" => array(
    "title-en" => "Technical reports",
    "title-fr" => "Rapports",
    "rank" => 50,
    ),
  "*" => array(
    "title-en" => "Miscellaneous/Submitted",
    "title-fr" => "Divers/Soumis",
    "rank" => 1000,
    ),
  );


class BibItem {
    var $authorFirstNames=array();
    var $authorLastNames=array();
    var $pubdate="";
    var $wridate="";
    var $subdate="";
    var $title="";
    var $journal="";
    var $conference_title = "";
    var $id = "";
    var $type = "";
    var $arxiv = "";
    var $doi = "";
    var $pubmed = "";
    var $halurl = "";
    var $data = array();
    var $opts;

    function BibItem($id,$halurl,$type,$xml,$opts)
    {
      $this->id = $id;
      $this->halurl = $halurl;
      $this->type = $type;
      $this->opts = $opts;
      $this->loadFromSimpleXMLElement($xml);
    }

    function getType()
    {
      return $this->type;
    }

    function getTypeRank()
    {
      global $TypesPublis;
      if (isset($TypesPublis[$this->type]))
      {
        $arr = $TypesPublis[$this->type];
        return $arr["rank"];
      }
      else return 10000000;
    }

    function getTypeTitle()
    {
      global $TypesPublis;
      if (isset($TypesPublis[$this->type]))
      {
        $arr = $TypesPublis[$this->type];
      }
      else
      {
        $arr = $TypesPublis["*"];
      }
      return $arr["title-".$this->opts->getLanguage()];
    }

    function getVal($xml)
    {
      return (string)$xml;
    }

    function loadFromSimpleXMLElement($xml,$inAuthors=0)
    {
      $tag = $xml->getName();
      if (($tag=="firstname") and $inAuthors)
      { $this->authorFirstNames[] = $this->getVal($xml); }
      else if (($tag=="lastname") and $inAuthors)
      { $this->authorLastNames[] = $this->getVal($xml); }
      else if ($tag=="publication_date")
      { $this->pubdate = str_replace("/","-",$this->getVal($xml)); }
      else if ($tag=="defence_date")
      { $this->pubdate = str_replace("/","-",$this->getVal($xml)); }
      else if ($tag=="writing_date")
      { $this->wridate = str_replace("/","-",$this->getVal($xml)); }
      else if ($tag=="production_date")
      { $this->wridate = str_replace("/","-",$this->getVal($xml)); }
      else if ($tag=="submission_date")
      { $this->subdate = str_replace("/","-",$this->getVal($xml)); }
      else if ($tag=="related_object")
      {
         $ty=$xml["id_type"];
         if ($ty=="doi")
         { $this->doi = str_replace("/","/",$this->getVal($xml)); }
         else if ($ty=="pubmed")
         { $this->pubmed = str_replace("/","-",$this->getVal($xml)); }
         else if ($ty=="arxiv")
         { $this->arxiv = str_replace("/","-",$this->getVal($xml)); }
      }
      else if ($tag=="file_url")
      {
        $type_url = "file_url_".(string)$xml["type"];
        $this->data[$type_url] = $this->getVal($xml);
      }
      else if ($tag=="authors")
      { $inAuthors = 1; }
      else if (count($xml->children())==0)
      {
        $this->data[$tag] = $this->getVal($xml);
      }
      foreach($xml->children() as $child)
      {
        $this->loadFromSimpleXMLElement($child,$inAuthors);
      }
    }

    function getAuthors($mainAuthors = array())
    {
      $result = "";
      $nb = 0;
      foreach($this->authorLastNames as $index => $lastname)
      {
        $firstname = $this->authorFirstNames[$index];
        $firstnameAbbr = "";
        foreach(preg_split("/[-]+/", $firstname) as $token)
        {
          if (!$firstnameAbbr=="")
            $firstnameAbbr .= "-";
          $firstnameAbbr .= substr($firstname,0,1).".";
        }
        if ($nb>0)
        {
          $result .= ", ";
          if ($nb==(count($this->authorFirstNames)-1))
          $result .= "and ";
        }
        if (isset($mainAuthors["$firstname $lastname"]))
          $result .= "<span class=\"mainAuthor\">$firstnameAbbr $lastname</span>";
        else
          $result .= "$firstnameAbbr $lastname";
        $nb += 1;
      }
      return $result;
    }
    function getYear()
    {
      $str = $this->pubdate;
      if ($str=="")
      {
         $str = $this->wridate;
         if ($str=="")
         {
           $str = $this->subdate;
         }
      }
      while(substr_count($str,"-")<2)
      { $str = "01-".$str;}
      $timestamp = strtotime($str);;
      return date("Y",$timestamp);
    }

    function getField($name,$prefix="",$suffix="",$default="")
    {
      if (isset($this->data[$name]))
      {
        return $prefix.$this->data[$name].$suffix;
      }
      return $default;
    }

    function getSummary()
    {
      $conference = $this->getField("conference_title", "Proceedings of ",",");
      $sconference = $this->getField("conference_title", "Presentation at ",", ");
      $journal = $this->getField("journal");
      $city = $this->getField("city",""," - ");
      $country = $this->getField("country","",", ");
      $volume = $this->getField("volume");
      $issue = $this->getField("issue","(",")");
      $refPubli = $this->getField("volume","<span class=\"bib_volume\">","</span>$issue:");
      $page = $this->getField("page","",", ", "To appear");
      $pages = $this->getField("page","",", ", "");
      $booktitle = $this->getField("book_title","",", ", "");
      $publisher = $this->getField("publisher_name","",", ", "");
      $year = $this->getYear();
      $type_report = $this->getField("type_report","","", "");
      $internal_reference = $this->getField("internal_reference","",", ", "");
      $university = $this->getField("university","",", ");



      if ($this->type=="document_ART_ACL")
      {
        return "<span class=\"bib_journal\">$journal</span>, $refPubli<span class=\"bib_pages\">$page</span> <span class=\"bib_year\">$year</span>";
      }
      else if ($this->type=="document_COMM_ACT")
      {
        return "<span class=\"bib_conference\">$conference</span> <span class=\"bib_city\">$city</span><span class=\"bib_country\">$country</span><span class=\"bib_volume\">$volume</span><span class=\"bib_number\">$issue</span><span class=\"bib_pages\">$pages</span><span class=\"bib_year\">$year</span>";
      }
      else if ($this->type=="document_COMM_SACT")
      {
        return "<span class=\"bib_conference\">$sconference</span> <span class=\"bib_city\">$city</span><span class=\"bib_country\">$country</span><span class=\"bib_year\">$year</span>";
      }
      else if ($this->type=="document_REPORT")
      {
         return "<span class=\"bib_type_report\">$type_report</span> <span class=\"bib_internal_reference\">$internal_reference</span><span class=\"bib_year\">$year</span>";
      }
      else if ($this->type=="document_COVS")
      {
        return "<span class=\"bib_booktitle\">$booktitle</span> <span class=\"bib_publisher\">$publisher</span><span class=\"bib_pages\">$pages</span><span class=\"bib_year\">$year</span>";
      }
      else if ($this->type=="document_CONF_INV")
      {
        return "<span class=\"bib_conference\">$sconference</span> <span class=\"bib_city\">$city</span><span class=\"bib_country\">$country</span><span class=\"bib_year\">$year</span>";
      }
      else if ($this->type=="document_THESE")
      {
        return "<span class=\"bib_university\">$university</span> <span class=\"bib_year\">$year</span>";
      }
	  
    }

    function getData($mainAuthors="")
    {
      $this->data["year"] = $this->getYear();
      $this->data["authors"] = $this->getAuthors($mainAuthors);
      $this->data["summary"] = $this->getSummary();
      $this->data["type"] = $this->getType();
      $this->data["type_rank"] = $this->getTypeRank();
      $this->data["type_title"] = $this->getTypeTitle();
      $this->data["pdf_preprint"] = $this->getField("file_url_PDF"," <a href=\"" , "\" target=\"_blank\">[pdf]</a>","");
      if ($this->doi != "")
      { $this->data["doi"] = "<a href=\"http://dx.doi.org/".$this->doi."\" target=\"_blank\">[doi]</a>";}
      if ($this->halurl != "")
      { $this->data["hal"] = "<a href=\"".$this->halurl."\" target=\"_blank\">[hal]</a>";}
      if ($this->pubmed != "")
      { $this->data["pubmed"] = "<a href=\"http://www.ncbi.nlm.nih.gov/pubmed/".$this->pubmed."\" target=\"_blank\">[pubmed]</a>";}
      if ($this->arxiv != "")
      { $this->data["arxiv"] = "<a href=\"http://arxiv.org/abs/".$this->arxiv."\" target=\"_blank\">[arxiv]</a>";}
      return $this->data;
    }

}


function postTreatBiblio($xml,$level,$opts,&$result)
{
  $tag = $xml->getName();
  if (startswith($tag,"document_"))
  {
    $arr = $xml->attributes();
    $id = $arr["uniqId"];
    $halurl = $arr["url"];
    $result[] = new BibItem($id,$halurl,$tag,$xml,$opts);
  }
  else
  {
    foreach($xml->children() as $child)
    {
      postTreatBiblio($child,$level+1,$opts,$result);
    }
  }
}

function loadBiblio($XMLFile,$template,$mainAuthors,$opts)
{
  $xml = simplexml_load_string(getExternalFileContent($XMLFile),'SimpleXMLElement',LIBXML_NOCDATA);
  if ($xml)
  {
  $result = array();
  postTreatBiblio($xml,0,$opts,$result);
  // Sort ...
  $years = array();
  $types = array();
  foreach($result as $id => $bib)
  {
    $years[$id] = $bib->getYear();
    $types[$id] = $bib->getTypeRank();
  }
  $sortByYear = 1;
  if ($sortByYear)
  { array_multisort( $years, SORT_DESC, $types, SORT_ASC, $result); }
  else 
  { array_multisort( $types, SORT_ASC, $years, SORT_DESC, $result); }
  $prevType = "";
  $prevYear = -1;
  foreach($result as $id => $bib)
  {
      $data = $bib->getData($mainAuthors);

      if ($data["year"]==$prevYear)
      {$data["year_caption"] = "";}
      else
      {
        $prevYear = $data["year"];
        $data["year_caption"] = $data["year"];
        $template->assign_block_vars("date",array("year"=>$data["year_caption"]));
        $prevType = "";
      }
      if ($data["type"]==$prevType)
      {
        $data["type_title"] = "";
      }
      else
      {
        $prevType = $data["type"];
        $data["category"] = $data["type_title"];
        $template->assign_block_vars("date.category",array("title"=>$data["type_title"]));
      }

      $template->assign_block_vars("date.category.bibitem",$data);
  }
  }
  else
  {
      $template->assign_vars( array("errormsg" => "Missing bibliographic items provider (HAL server might be down). Please come back a tad later."));
  }
}



function loadDynamicContents($XMLFile,$opts,$limitLow=-1,$limitHigh=PHP_INT_MAX)
{
  $result = array();
  $xml = simplexml_load_file($XMLFile,'SimpleXMLElement',LIBXML_NOCDATA);
  $nb = 0;
  foreach($xml->children() as $dynContentElement)
  {
    $dynContent = new DynContent($opts);
    $id= -1;
    $hidden = False;
    foreach($dynContentElement->children() as $field)
    {
      if (!strcmp($field->getName(),"id"))
      {
        $id = cleanup($field->asXML());
        $dynContent->setID($id);
      }
      else if (!strcmp($field->getName(),"hide"))
      {
        $hidden = True;
      }
      else
      {
        $dynContent->setData($field->getName(),html_entity_decode(cleanup($field->asXML())));
      }
    }
    if (!$hidden)
    {
      if (($nb>=$limitLow)&&($nb<$limitHigh))
      {
        $result[] = $dynContent;
      }
    }
    $nb += 1;
  }
  return $result;
}



?> 