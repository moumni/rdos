  function showElement(category,num)
  {
    $("#"+category+"Show"+num).hide();
    $("."+category+"Hide"+num).show();
    $("#"+category+"On"+num).show("normal");
    return false;
  };

  function hideElement(category,num)
  {
    $("#"+category+"On"+num).hide("normal");
    $("."+category+"Hide"+num).hide();
    $("#"+category+"Show"+num).show();
    return false;
  }
