<?php
    // Connect to DB
  require_once('php/db.php');
  require_once('php/config.inc.php');
  require_once('php/functions.php');
  require_once('php/template.php');
  require_once('php/dynContent.php');
  require_once('php/menu.php');
  require_once('php/utils.php');
  require_once('php/urlOptions.php');
  require_once('php/options.php');
  require_once('php/jobs.php');

  error_reporting(E_ALL);	
  ini_set( 'display_errors','1');
  
  $mainPage = 'viewjob.php';

  $action = "view";
  if (isset($_REQUEST['action']))
  { $action = $_REQUEST['action']; }
  $ajax = false;
  if (isset($_REQUEST['ajax']))
  { $ajax = $_REQUEST['ajax']; }
   
  $ajaxType = 'all';
  if (isset($_REQUEST['ajaxType']))
  { 
	$ajaxType = $_REQUEST['ajaxType']; 
  }
  

 $urlFormatOptions =  array(
    'jobid'=>'jobid',
	'lang'=>'lang',
	'css'=>'css',
  );

  
  // Retrieving and checking variables
  $opts = new URLOptions();
  $opts->assignOptions($_REQUEST);
  $opts->restrictTo($urlFormatOptions);
  $lang = $opts->getLanguage();
  $css = $cssFiles[$opts->getCSS()];
  $jobid = $opts->getOption('jobid');

  if (($action=="submitJob") and isset($_REQUEST['idgen']) and isset($_REQUEST['email']))
  {
	$jobid = addJob($dbh,$_REQUEST['idgen'],$_REQUEST['email'],$_REQUEST);
	$opts->setOption('jobid',$jobid);
	header('Location: '.str_replace("&amp;","&",$opts->getCurrentURL()));
	exit();
  }

  global $jobsDir;
  $jobdata = getJobData($dbh,$jobid);
  $gendata = getGeneratorData($dbh,$jobdata["idGenerator"]);
  $status = $jobdata["status"];
  $outputFormat = $gendata["outputFormat"];
  $directory = $jobdata["directory"];
  if ($status>=JOB_DONE)
  { $needsRefresh = 0; }
  else
  { $needsRefresh = 1; }
  
  
  $result = "N/A";
  
  
  $template = new Template('.');
  $template->set_filenames(array(
    'body' => $opts->locatePage('job'),
    'viewcontent' => $opts->locatePage('viewcontent'),
    'viewcontentstatus' => $opts->locatePage('viewcontentstatus'),
    'viewcontentresult' => $opts->locatePage('viewcontentresult'),
  ));
  
  if ($status==JOB_DONE)
  {
	$url = fetchResults($dbh,$jobid,$directory);
	$txt = "";
	if ($lang=="en")
	{ $txt = "Download Raw Output";}
	else if ($lang=="fr")
	{ $txt = "Télécharger tous les objets";}
	$result = '<span class="resultLink"><a href="'.$url.'" target="_blank">'.$txt.'</a></span>';
	formatResults($template,$url);
  }
  
  
  $template->assign_vars( array(
    'mainTitle' => "RDOS - Results for job#".$jobid,
    'jobid' => $jobid,
    'description' => $gendata["description-".strtoupper($lang)],
    'authors' => $gendata["authors"],
    'reference' => $gendata["reference"],
    'url' => $gendata["url"],
    'CSSFile' => $css,
	'result'  => $result,
	'outputFormat' => $outputFormat,
	'linkEN' => $opts->formatUpdatedURLSingle('lang','en'),
    'linkFR' => $opts->formatUpdatedURLSingle('lang','fr'),
	'params' => formatParameters($jobdata["parametersJSON"]),
	'command' => formatCommandLine($dbh,$jobdata,$gendata),
	'status' => formatStatus($status,$lang),
	'currentURL' => $opts->getCurrentURL(),
	'needsRefresh' => $needsRefresh,
	'HTMLRefresher' => ($needsRefresh? "<meta id=\"refresher\" http-equiv=\"refresh\" content=\"5;URL=".$opts->getCurrentURL()."\">":""),
  )
  ) ;
  
 
  $template->assign_var_from_handle('content', 'viewcontent');
  if ($ajax)
  {
	sleep(1);
	if ($ajaxType=="all")
	{ $template->pparse('viewcontent');}
	else if ($ajaxType=="status")
	{ $template->pparse('viewcontentstatus');}
	else if ($ajaxType=="result")
	{ $template->pparse('viewcontentresult');}
  }
  else
  {
	$template->pparse('body');
  }


?>